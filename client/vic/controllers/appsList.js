// Controller
//
// It requires the "client" service, and fetches information about the server,
// it adds either an error or info about the server to $scope.
//
// It also requires the esFactory to that it can check for a specific type of
// error which might come back from the client

angular.module("vic").controller("AppsListCtrl", ['$scope', 'client', 'esFactory', '$meteor',
  function($scope, client, esFactory, $meteor){

  // client.ping({
  //   requestTimeout: 30000,

  //   // undocumented params are appended to the query string
  //   hello: "elasticsearch!"
  // }, function (error) {
  //   if (error) {
  //     console.error('elasticsearch cluster is down!');
  //   } else {
  //     console.log('Ping: All is well');
  //   }
  // });

  // client.cluster.state({
  //   metric: [
  //     'cluster_name',
  //     'nodes',
  //     'master_node',
  //     'version'
  //   ]
  // })
  // .then(function (resp) {
  //   $scope.clusterState = resp;
  //   $scope.error = null;
  // })
  // .catch(function (err) {
  //   $scope.clusterState = null;
  //   $scope.error = err;

  //   // if the err is a NoConnections error, then the client was not able to
  //   // connect to elasticsearch. In that case, create a more detailed error
  //   // message
  //   if (err instanceof esFactory.errors.NoConnections) {
  //     $scope.error = new Error('Unable to connect to elasticsearch. ' +
  //       'Make sure that it is running and listening at http://localhost:9200');
  //   }
  // });    
  

  client.search({
    index: 'play_store_v2',
    type: 'apps'        
    // q: 'developer_website:https://www.google.com/url?q=http://www.dream-up.eu/game&sa=D&usg=AFQjCNFvSQxfbYdVr8zAGzHMFUZQ9XM31g'
  }).then(function (resp) {
      var hits = resp.hits;
      console.log("hits: " + hits);
      console.log(hits.hits);
      //hits = hits.hits[1]._source.results[0];
      $scope.clusterState = hits;
      $scope.error = null;  
  }, function (err) {
      console.trace(err.message);
  });
}]);