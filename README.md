Angular-Meteor Elasticsearch
===================
Angular-Meteor brings the responsive power of Angular to the powerful and flexible Meteor stack. 
Includes the official low-level Elasticsearch client (for Node.js) for meteor server side.

### Installation

Add the AngularJS package to Meteor
```bash
meteor add urigo:angular
```

Includes the official low-level Elasticsearch client for Node.js
```bash
meteor add bigdata:elasticsearch
```

